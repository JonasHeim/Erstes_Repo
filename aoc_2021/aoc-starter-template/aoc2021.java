
/**
 * Einfache Grundklassse für den TakeOff für den Advent of Code 2021
 * 
 * @author Frank Schiebel
 * @version 0.1
 */

import java.io.FileReader;
import java.util.Arrays;
import com.opencsv.CSVReader;
import java.util.ArrayList;
import java.util.Iterator;

public class aoc2021
{

    // Eingabe wird in eine ArrayList von String-Arrays ("input") eingelesen.
    // Das muss man je nach Eingabeformat und Aufgabenstellung sicherlich 
    // weiterverarbeiten und in eine andere Datenstruktur überführen.
    ArrayList<String[]> input = new ArrayList<String[]>();

    /**
     * Konstruktor für Objekte der Klasse aoc2021
     * 
     * @param  filename      Dateiname des Rätsel-Inputs (z.B. "day0") 
     *                    im Projektverzeichnis.
     **/
    public aoc2021(String filename) throws Exception {
        this.readInput(filename);
        this.printInput();

    }

    /**
     * Beispielhafte Methode zum Einlesen von CSV Daten
     * in die ArrayList von String Arrays ("input").
     *  
     * Dokumentation: http://opencsv.sourceforge.net/
     **/
    public void readInput(String filename) throws Exception {
        CSVReader reader = new CSVReader(new FileReader(filename));
        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line
            input.add(nextLine);
        }
    }

    /**
     * Kontrollausgabe der input ArrayList.
     **/
    public void printInput() {

        // Ausgabe der Input-Datei
        Iterator<String[]> iter = input.iterator();
        int lnum = 0;
        int maxFN = 0;
        int minFN = 0;

        while(iter.hasNext()) {
            String[] line=iter.next();

            int lineFNum = line.length;
            for (int i=0;i<lineFNum;i++) {
                System.out.println("[" + lnum + "][" + i + "] " + line[i] + " ");
            }
            lnum++;
        }

        System.out.println("");
        System.out.println("___ Info _______");
        System.out.println(lnum + " Zeilen");
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    (Beschreibung des Parameters)
     * @return        (Beschreibung des Rückgabewertes)
     */
    public void sonarSweep()
    {
        Iterator<String[]> iter = input.iterator();
        int lnum = 0;
        int maxFN = 0;
        int minFN = 0;
        int increases = 0;
        int a = 1000000000;
        int v;
        while(iter.hasNext()) {

            v = a;

            String[] line=iter.next();

            int lineFNum = line.length;
            for (int i=0;i<lineFNum;i++) {
                System.out.println("[" + lnum + "][" + i + "] " + line[i] + " hallo");
                a = Integer.parseInt(line[i]);
                if (a > v){
                    increases++;
                    System.out.println("increases");
                }
            }
            lnum++;

        }
        System.out.println("");
        System.out.println("___ Info _______");
        System.out.println(lnum + " Zeilen");
        System.out.println(increases + " increases");
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    (Beschreibung des Parameters)
     * @return        (Beschreibung des Rückgabewertes)
     */
    public void sonarSweep2()
    {
        Iterator<String[]> iter = input.iterator();
        int lnum = 0;
        int maxFN = 0;
        int minFN = 0;
        int increases = 0;
        int a = 1000000;
        int v = 10000000;
        int v2 = 10000000;
        int v3;
        while(iter.hasNext()) {
            v3 = v2;
            v2 = v;
            v = a;

            String[] line=iter.next();

            int lineFNum = line.length;
            for (int i=0;i<lineFNum;i++) {
                System.out.println("[" + lnum + "][" + i + "] " + line[i] + " hallo");
                a = Integer.parseInt(line[i]);
                System.out.println(a + v + v2);
                if (v + v2 + v3 < a + v + v2){
                    increases++;
                    System.out.println("increases");
                }
            }
            lnum++;

        }
        System.out.println("");
        System.out.println("___ Info _______");
        System.out.println(lnum + " Zeilen");
        System.out.println(increases + " increases");
    }

    /**
     * tag 2 dive
     **/
    public void dive() {

        // Ausgabe der Input-Datei
        Iterator<String[]> iter = input.iterator();
        int lnum = 0;
        int maxFN = 0;
        int minFN = 0;
        int x = 0;
        int y = 0;
        char v;
        while(iter.hasNext()) {
            String[] line=iter.next();

            int lineFNum = line.length;
            for (int i=0;i<lineFNum;i++) {
                System.out.println("[" + lnum + "][" + i + "] " + line[i] + " ");
                v = line[i].charAt(0);
                System.out.print(v);
                switch(v){
                    case 'f':
                        System.out.print(line[i].charAt(8));
                        x = x + Character.getNumericValue(line[i].charAt(8));
                        System.out.print(" x ist " + x);
                        break;
                    case 'u':
                        System.out.print(line[i].charAt(3));
                        y = y - Character.getNumericValue(line[i].charAt(3));
                        System.out.print(" y ist " + y);
                        break;
                    case 'd':
                        System.out.print(line[i].charAt(5));
                        y = y + Character.getNumericValue(line[i].charAt(5));
                        System.out.print(" y ist " + y);
                        break;

                }
                System.out.println("");
            }

            lnum++;
        }

        System.out.println("");
        System.out.println("___ Info _______");
        System.out.println(lnum + " Zeilen");
        System.out.println(x + " horizontal");
        System.out.println(y + " depth");
        System.out.println(x * y + " multiplicated");
    }

    /**
     * tag 2 dive part Two
     **/
    public void dive2() {

        // Ausgabe der Input-Datei
        Iterator<String[]> iter = input.iterator();
        int lnum = 0;
        int maxFN = 0;
        int minFN = 0;
        int x = 0;
        int y = 0;
        int aim = 0;
        char v;
        while(iter.hasNext()) {
            String[] line=iter.next();

            int lineFNum = line.length;
            for (int i=0;i<lineFNum;i++) {
                System.out.println("[" + lnum + "][" + i + "] " + line[i] + " ");
                v = line[i].charAt(0);
                System.out.print(v);
                switch(v){
                    case 'f':
                        System.out.print(line[i].charAt(8));
                        x = x + Character.getNumericValue(line[i].charAt(8));
                        System.out.print(" x ist " + x);
                        y = y + aim * Character.getNumericValue(line[i].charAt(8));
                        System.out.print(" y ist " + y);
                        break;
                    case 'u':
                        System.out.print(line[i].charAt(3));
                        aim = aim - Character.getNumericValue(line[i].charAt(3));
                        System.out.print(" y ist " + y);
                        break;
                    case 'd':
                        System.out.print(line[i].charAt(5));
                        aim = aim + Character.getNumericValue(line[i].charAt(5));
                        System.out.print(" y ist " + y);
                        break;

                }
                System.out.println("");
            }

            lnum++;
        }

        System.out.println("");
        System.out.println("___ Info _______");
        System.out.println(lnum + " Zeilen");
        System.out.println(x + " horizontal");
        System.out.println(y + " depth");
        System.out.println(x * y + " multiplicated");
    }

    /**
     * tag 3 Binary Diagnostic
     **/
    public void binaryDiagnostic() {

        // Ausgabe der Input-Datei
        Iterator<String[]> iter = input.iterator();
        int lnum = 0;
        int maxFN = 0;
        int minFN = 0;
        int anzahl = 0;
        int gamma = 0;
        int epsilon = 0;
        int[] binary = new int[12];
        boolean[] mehrzahl = new boolean[12];
        while(iter.hasNext()) {
            String[] line=iter.next();

            int lineFNum = line.length;
            for (int i=0;i<lineFNum;i++) {
                System.out.println("[" + lnum + "][" + i + "] " + line[i] + " ");

                for (int o = 0; o < line[i].length(); o++){

                    if (line[i].charAt(o) == '1'){
                        binary[o]++;
                    }
                }

            }

            lnum++;
        }

        System.out.println("");
        System.out.println("___ Info _______");
        System.out.println(lnum + " Zeilen");
        for(int o = 0; o < 12; o++){
            if(binary[o] == 0){
                break;
            }
            System.out.println(o+1 + " werte " + binary[o]);
            mehrzahl[o] = (double)binary[o]/lnum > 0.5;
            System.out.println(mehrzahl[o] + " mehrzahl " + o+1);
            anzahl++;
        }
        int o = 0;
        for(int i = anzahl-1; i >= 0; i--){
            int a = (int)Math.pow(2, o);
            if(mehrzahl[i]){
                gamma = gamma + a;
            }else{
                epsilon = epsilon + a;
            }
            o++;
        }
        System.out.println(gamma + " Gamma");
        System.out.println(epsilon + " Epsilon");
        System.out.println(gamma * epsilon + " multipliziert");
    }

    /**
     * tag 3 Binary Diagnostic part 2
     **/
    public void binaryDiagnostic2() {

        // Ausgabe der Input-Datei
        Iterator<String[]> iter = input.iterator();
        String[] line2=iter.next();
        int laenge = line2[0].length();
        int lnum = 0;
        int maxFN = 0;
        int minFN = 0;
        iter = input.iterator();
        int binary = 0;
        char[] crit = new char[laenge];
        String oxygen = "ox";
        String co2 ="co2";
        //boolean[] mehrzahl = new boolean[laenge];
        boolean weiter = true;
        for(int l = 0;l <= laenge; l++){
            binary = 0;
            iter = input.iterator();
            while(iter.hasNext()) {
                String[] line=iter.next();
                for(int p = 0; p < l; p++){
                    if(line[0].charAt(p) != crit[p]){
                        //System.out.println("break " + p);
                        weiter = false;
                        break;
                    }
                    //System.out.println("passt");
                }
                if (weiter){
                    int lineFNum = line.length;
                    for (int i=0;i<lineFNum;i++) {
                        System.out.println("[" + lnum + "][" + i + "] " + line[i] + " ");
                        if (l != laenge){
                            if (line[i].charAt(l) == '1'){
                                binary++;
                            }
                        }else{
                            oxygen = line[i];
                        }

                    }
                    lnum++;
                }
                weiter = true;
            }
            if(l != laenge){
                if ( (double)binary/lnum >= 0.5){
                    crit[l] = '1';
                }else{
                    crit[l] = '0';
                }
            }
            lnum = 0;
        }
        crit = new char[laenge];
        for(int l = 0;l <= laenge; l++){
            binary = 0;
            iter = input.iterator();
            while(iter.hasNext()) {
                String[] line=iter.next();
                for(int p = 0; p < l; p++){
                    if(line[0].charAt(p) != crit[p]){
                        //System.out.println("break " + p);
                        weiter = false;
                        break;
                    }
                    //System.out.println("passt");
                }
                if (weiter){
                    int lineFNum = line.length;
                    for (int i=0;i<lineFNum;i++) {
                        System.out.println("[" + lnum + "][" + i + "] " + line[i] + " ");
                        if (l != laenge){
                            if (line[i].charAt(l) == '1'){
                                binary++;
                                System.out.println("zaehler " + binary);
                            }
                        }else{
                            co2 = line[i];
                        }

                    }
                    lnum++;
                }
                weiter = true;
            }
            if(l != laenge){
                if ( ((double)binary/lnum >= 0.5||(double)binary/lnum == 0)&& (double)binary/lnum != 1){
                    crit[l] = '0';
                    //System.out.println("0" + l);
                }else{
                    crit[l] = '1';
                    //System.out.println("1" + l);
                }
            }

            lnum = 0;
            //System.out.println("hallo" + l);
        }
        System.out.println("");
        System.out.println("___ Info _______");
        System.out.println(lnum + " Zeilen");
        System.out.println(oxygen + " oxygen");
        System.out.println(Integer.parseInt(oxygen,2) + " oxygen als dezimal");
        System.out.println(co2 + " co2");
        System.out.println(Integer.parseInt(co2,2) + " co2 als dezimal");
        System.out.println(Integer.parseInt(oxygen,2) * Integer.parseInt(co2,2)+ " multipliziert");

    }

    /**
     * tag 3 Giant Squid
     **/
    public void giantSquid() {

        // Ausgabe der Input-Datei
        Iterator<String[]> iter = input.iterator();
        String[] werte=iter.next();
        // for (int i = 0; i<werte.length; i++){
        // System.out.print("[" + i + "] " + werte[i] + " ");
        // }
        String testen = "-1";
        ArrayList<boolean[]> treffer = new ArrayList<boolean[]>();
        int lnum = 0;
        int maxFN = 0;
        int minFN = 0;
        int bingoSieger = -1;
        int durchgang = 0;
        while(iter.hasNext()) {
            String[] line=iter.next();
            boolean[] hallo = new boolean[5];
            treffer.add(hallo);
        }
        while(bingoSieger == -1){
            iter = input.iterator();
            iter.next();
            if (werte[durchgang].length() == 1){
                testen = " " + werte[durchgang];
            }else{
                testen = werte[durchgang];
            }
            while(iter.hasNext()) {
                String[] line=iter.next();

                int lineFNum = line.length;
                for (int i=0;i<lineFNum;i++) {
                    System.out.print("[" + lnum + "][" + i + "] " + line[i] + " ");
                    if(line[i].indexOf(testen) != -1 && line[i].indexOf(testen)%3 == 0){
                        //System.out.print("index = " + line[i].indexOf(testen) + " string = " + testen + " wert = " + (line[i].indexOf(testen)/3+1));
                        boolean[] marked = treffer.get(lnum);
                        marked[(line[i].indexOf(testen)/3)] = true;
                    }else if(line[i].indexOf(" " + testen) != -1 && line[i].indexOf(" "+testen)%3 == 2){
                        //System.out.print("index = " + line[i].indexOf(" " + testen) + " string = " + testen + " wert = " + ((line[i].indexOf(" " + testen)+1)/3+1));
                        boolean[] marked = treffer.get(lnum);
                        marked[((line[i].indexOf(testen)+1)/3)] = true;
                    }
                    System.out.print(" ");
                    for(int o = 0; o < 5; o++){
                        boolean[] marked = treffer.get(lnum);
                        if (marked[o]){
                            System.out.print(" o" );
                        }else{
                            System.out.print(" x" );
                        }

                    }
                    System.out.println();
                }

                lnum++;
            }

            for(int i = 0; i <lnum; i++){
                boolean[] marked = treffer.get(i);

                if(marked[0]&&marked[1]&&marked[2]&&marked[3]&&marked[4]){
                    System.out.println(i + " hier");
                    bingoSieger = i-((i-1)%6);
                    System.out.println(bingoSieger + " bingoSieger");
                }
                if((i-1)%6 == 0){
                    boolean[] marked2 = treffer.get(i+1);
                    boolean[] marked3 = treffer.get(i+2);
                    boolean[] marked4 = treffer.get(i+3);
                    boolean[] marked5 = treffer.get(i+4);
                    for(int x = 0;x<5;x++){
                        if(marked[x]&&marked2[x]&&marked3[x]&&marked4[x]&&marked5[x]){
                            bingoSieger = i;
                            System.out.println(bingoSieger + " bingoSieger");
                        }
                    }
                }

            }
            lnum = 0;

            System.out.println(durchgang + " Durchgang");
            System.out.println(testen + " Testen");
            System.out.println("////////////////////////" );
            durchgang++;
        }
        iter = input.iterator();
        for(int i = 0; i <= bingoSieger; i++){
            iter.next();
        }
        int nichtGezogene = 0;
        for(int x = 0; x < 5; x++){
            String[] line = iter.next();
            String line2 = line[0];
            System.out.println(line2);
            boolean[] marked = treffer.get(bingoSieger + x);
            for(int y = 0; y < 5; y++){
                if(!marked[y]){
                    String s = "";
                    
                    if(line2.charAt(y*3) != ' '){
                        
                        s = s +line2.charAt(y*3);
                    }
                    s = s +line2.charAt(y*3+1);
                    nichtGezogene = nichtGezogene + Integer.parseInt(s);

                }

            }
        }
        System.out.println("");
        System.out.println("___ Info _______");
        System.out.println(lnum + " Zeilen");
        System.out.println(bingoSieger + " bingoSieger");
        System.out.println(nichtGezogene + " nichtGezogen");
        System.out.println(testen + " Testen");
        System.out.println(durchgang + " durchgang");
        System.out.println(werte[durchgang-1] + " werte");
        System.out.println(nichtGezogene * Integer.parseInt(werte[durchgang-1]) + " multipliziert");

    }
}
