
/**
 * Beschreiben Sie hier die Klasse Uebungen01.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Uebungen01
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    //private int x;

    /**
     * Konstruktor für Objekte der Klasse Uebungen01
     */
    public Uebungen01()
    {
        // Instanzvariable initialisieren
        //x = 0;
    }

    /**
     * Modulo: Berechne a mod b "von hand"
     * 
     * @param  a      Zahl die geteilt werden soll
     * @param  b      Zahl durch die geteilt wird.
     * @return        Rest der ganzzahligen Division 
     */
    public int MyModulo(int a, int b)
    {
        int remainder=0;
        // dein Code
        remainder=a-(a/b)*b;

        // Rückgabe
        return remainder;
    }

    /**
     * Switch: Vertausche a und b      
     * 
     * @param  a      Zahl 1
     * @param  b      Zahl 2
     */
    public void Switch(int a, int b) 
    {
        int a2=a;
        a=b;
        b=a2;
        System.out.print("a =" + a + " b = " + b);
    }

    /**
     * Pyramide: Volumen einer Pyramide      
     * 
     * @param  g     Grundfläche der Pyramide
     * @param  h     Höhe der Pyramide
     * @return       Voulmen der Pyramide
     */
    public float Pyramide(float g, float a) {
        float volumen = 2;
        volumen = g*a*a/3;
        return volumen;
    }

    /**
     * Alterstest: Testet das Alter    
     * 
     * @param alter
     * ....
     * @return 
     */
    public void Alterstest(int alter) {
        if (alter < 7){
            System.out.println("Geschäftsunfähig");
        }else if (alter < 18){
            System.out.println("Geschäftsfähigkeit beschränkt");
        }else{
            System.out.println("unbeschränkte Geschäftsfähigkeit");
        }

    }

    // Erstelle weitere Methoden
    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    die zahll die überprüft werdern soll
     */
    public void GeradeOderUngerade(int y)
    {
        if ((y/2)*2 == y){
            System.out.println("Die Zahl "+y+" ist gerade");
        }else{
            System.out.println("Die Zahl "+y+" ist ungerade");
        }
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  n    note die überprüft wird
     * @return        die ganze note
     */
    public void Schulnoten(double n)
    {
        n=n+0.5;
        int n2 =(int)n;

        switch (n2){
            case 1:
                System.out.println("sehr Gut");
                break;
            case 2:
                System.out.println("Gut");
                break;
            case 3:
                System.out.println("befriedigend");
                break;
            case 4:
                System.out.println("ausreichend");
                break;
            case 5:
                System.out.println("mangelhaft");
                break;
            case 6:
                System.out.println("ungenügen");
                break;
            default:
                System.out.println("keine gültige Note");
                break;
        }

    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    (Beschreibung des Parameters)
     * @return        (Beschreibung des Rückgabewertes)
     */
    public void Wertetabelle()
    {
        for(float i = -2; i<=2;i=i+(float)0.1){

            System.out.println(i*i-6*i-4);
        }
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y	Das Jahr, dass überprüft wird
     * @return		ob das Jahr ein schaltjahr ist
     */
    public boolean Schaltjahr(int y)
    {
        if (y % 4 == 0){
            if (y % 100 == 0){
                if (y % 400 == 0){
                    return true;
                }
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y	zahl, die überprüft werden soll
     * @return		anzahl der stellen
     */
    public int Stellenzähler(int y)
    {
        int n = 0;
        while (y != 0){
            n++;
            y = y/10;
        }
        return n;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y	zahl, die überprüft werden soll
     * @return		die Quersumme
     */
    public int Quersumme(int y)
    {
        int n = 0;
        int q = 0;
        while (y != 0){
            q = q + y %10;
            y = y/10;
        }

        return q;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y	die Dualzahl
     * @return		die Dezimalzahl
     */
    public int DualZuDezimal(int y)
    {
        int d = 0;
        int h = 1;
        while (y != 0){
            if (y % 10 == 1){
                d = d + h;
            }
            h = h*2;
            y = y/10;
        }
        return d;
    }
    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y	die Zahl, die umgedreht werden soll
     * @return		die Umgetrete Zahl
     */
    public int Zahlendreher(int y)
    {
        int d = 0;
        int h = 1;
        while (y != 0){
            d = d*10 + y % 10;
            y = y/10;
        }
        return d;
    }
    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  n	anzahl der Iterationtn
     * @return		pi
     */
    public double PiNachLeibnitz(int n)
    {
        double pi = 0;
        boolean plus = true;
        for (int i = 1; i<=n; i++){
            if (i%2 != 0){
                if (plus){
                    pi = pi + 1.0/i;
                    plus =false;
                }else{
                    pi = pi - 1.0/i;
                    plus = true;
                }
            }
        }
        pi = pi *4 ;
        return pi;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  s	spalten
     * @param  z        zeilen
     */
    public void XQuadrat(int s, int z){
        for (int i = 0; i<z;i++){
            System.out.println();
            for (int n = 0; n < s; n++){
                System.out.print("x");
            }
        }

    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  s	spalten und zeilen
     */
    public void Dreieck(int s){
        for (int i = 0; i<=s;i++){
            System.out.println();
            for (int n = 0; n < i; n++){
                System.out.print("x");
            }
        }

    }

    /**
     * eine Methode um lottozahlen auszugeben
     * 
     */
    public void Lottozahlen()
    {
        int[] bereitsGezogeneZahlen = new int[6];
        int n = 0;
        boolean u = true;
        while(n < 6){
            int r = (int) (Math.random()*(49-1)) + 1;
            u = true;
            for(int i = 0; i < n; i++){
                if (bereitsGezogeneZahlen[i] == r){
                    u = false;
                }
            }
            if(u){
                bereitsGezogeneZahlen[n] = r;
                n++;
            }
        }
        for(int i = 0; i < 6; i++){
            System.out.print(bereitsGezogeneZahlen[i]);
            System.out.print(" ");
        }
        System.out.println();
    }


}
